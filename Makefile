SHELL=/bin/bash
.SHELLFLAGS=-O extglob -c

all: help

help:
	@echo "Commands:"
	@echo "	help      - Display this help text"
	@echo "	build     - Builds a static version for deployment or local use"
	@echo "	setup     - Installs required node modules"
	@echo "	dev       - Run the development server locally"

# TODO: Move npm commands to 'package.json' file.
setup:
	npm install
	git submodule update --init --recursive --depth 1

build: setup
	hugo

dev: setup
	hugo server -w --disableFastRender -b localhost:1313
