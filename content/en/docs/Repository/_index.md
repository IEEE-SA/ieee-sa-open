---
title: "Repositories"
weight: 2
description: >
  IEEE SA Open Repositories
---

# Official SA Open Repositories

| Name                | Description                                           | URL                                         |
|---------------------|-------------------------------------------------------|---------------------------------------------|
| Infra Playbooks     | Ansible playbooks to deploy the IEEE SA Open Platform | https://opensource.ieee.org/infra/playbooks |
| Infra Documentation | Static site for the IEEE SA Open Platform             | https://gitlab.com/IEEE-SA/ieee-sa-open     |

# Working with a repository

## Cloning a repository

To clone a repository you must have a copy of (git)[https://git-scm.com/] installed on your host. To clone a repository simply use the following command (where URL is the repository URL from the table above):

`git clone URL`

Once the repository has been cloned, use the text editor of your choice to inspect or edit the files inside the new directory created by *git*. The new directory will match the repository name. For example the URL `https://opensource.ieee.org/infra/playbooks` would be cloned to the *playbooks* directory in the current working directory from where the command was run.

## Creating a New Branch

Due to the multi-user nature of *git*, it is necessary to store ones changes in a new *branch*. A *branch* is a copy of contents of the repository taken from when the branch was created. This allows you to work on changes with out conflicting with other people working on the same repository.

You can create a new branch with the following command (replace *new_branch_name* with the name for your new branch):

`git branch new_branch_name`

Once the new branch is created you must switch to it with the following command:

`git checkout new_branch_name`

Alternatively you can create a new branch and check it out with a single command:

`git checkout -b new_branch_name`

## Committing changes

Once you have made changes to files in a repository, you will want to *commit* them. You can think of committing changes to *git* as saving them. When you commit files into a git repository you will be asked to make a "commit message". This "commit message" is stored along with your changes, and may help identify when changes where made to files in the repository.

Before commiting a file you will want to confirm what files have been updated. Do this with the *status* command:

`git status`

If the file you are intending to commit is listed, you may add its changes to the repository. Run the following command to add the files you would like to commit:

`git add FILENAME`

Commit the file to the repository:

`git commit -m "Your commit message here"`

## Pushing and Pulling Changes

STUB

## Merging Changes

STUB
