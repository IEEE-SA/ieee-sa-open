---
title: "3.1 [DRAFT] Backup Plan"
weight: 1
description: >
  Framework for conducting backups and and restoring the IEEE SA Open Platform.
---

The following core components of the AWS infrastructure must be backed up:

* RDB/PostgreSQL
* GitLab Application
* EBS File System:
  * sgit-app
  * sgit-gitaly
* S3 Buckets:
  * sgit-backups
  * sgit-gitlab
  * sgit-registry
  
  ![Backup Visualization](../backup_flow.png)

## Databases

Backups on the PostgeSQL Database are to run as follows:

* A read-only RDB instance is to be created in the AWS region *US-WEST*
* Via the AWS Console, enable replication from production to *US-WEST*

## GitLab Application

GitLab backups are to be enabled to an S3 Bucket stored in AWS's *US-WEST* region.

Documentation for enabling backups can be found in [GitLab's Documentation](https://docs.gitlab.com/ee/raketasks/backup_restore.html#using-amazon-s3)


## Shared File Systems

All EBS file systems should be replicated to read-only copy in *US-WEST*

## S3 Buckets

All S3 should be replicated to read-only copy in *US-WEST*

The following S3 Buckets should be backed up:
  * sgit-backups
  * sgit-gitlab
  * sgit-registry

# Restore

## Enable the Fail-Over environment

Start EC2 instances 
https://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-for-omnibus-gitlab-installations
