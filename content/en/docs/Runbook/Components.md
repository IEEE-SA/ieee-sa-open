---
title: "3. Components"
weight: 1
description: >
  Basic components for GitLab
---

The following is a manifest of basic components required to build the SA Open Platform. As noted by the GitLab documentation technicians and administrators wanting to grasp a deeper understanding of GitLab should consider GitLab to be an amalgamation of the interactions of all the components outlined below.

For a fully detailed description on what each component does please see the official GitLab documentation's [Component List](https://docs.gitlab.com/ce/development/architecture.html#component-list). Only items in the list marked "_Installed by Default_" or "_Requires additional configuration, or GitLab Managed Apps_" are included in the IEEE SA Open Platform.

* Dashboards
  * [Grafana](https://grafana.com)
  * [Prometheus](https://prometheus.io/)
* Database:
  * [Redis](https://redis.io/)
  * [PostgreSQL](https://www.postgresql.org/)
* Email
  * [Dovcot](https://www.dovecot.org/)
* GitLab Core Components:
  * [Alertmanager](https://github.com/prometheus/alertmanager/blob/master/README.md)
  * [Certificate Manager](https://github.com/jetstack/cert-manager/blob/master/README.md)
  * [Gitaly](https://gitlab.com/gitlab-org/gitaly/blob/master/README.md)
  * [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/README.md)
  * [GitLab Shell](https://gitlab.com/gitlab-org/gitlab-shell/blob/master/README.md)
  * [GitLab Workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse/blob/master/README.md)
  * [Mattermost](https://mattermost.com/)
  * [NGINX](https://nginx.com)
  * [Praefect](https://gitlab.com/gitlab-org/gitaly/blob/master/README.md)
  * [Sidekiq](https://github.com/mperham/sidekiq/blob/master/README.md)
* Monitoring
  * [GitLab Exporter](https://gitlab.com/gitlab-org/gitlab-exporter)
  * [Node Exporter](https://github.com/prometheus/node_exporter/blob/master/README.md)
  * [Redis Exporter](https://github.com/oliver006/redis_exporter/blob/master/README.md)
* Requirements
  * [Ruby (MRI)](https://www.ruby-lang.org/en/)
  * [Go](https://golang.org/)
  * [Git](https://git-scm.com/)
  * [Node.js](https://nodejs.org/en/)
  * [Open SSH Server](https://openssh.com/)
