---
title: "1. Install"
weight: 1
description: >
  Install Process for SA Open Platform
---

When installing the SA Open Platform, we recommend you maintain a CIS Hardened Instance of Ubuntu 18.04 in order to run the Ansible Playbooks from

## Configure the Ansible Host

When running the playbooks it is recommend to use a dedicated EC2 instance to run the playbooks from. This instance will require the following packages:

* [ansible](https://www.ansible.com/)
* [git](https://git-scm.com/)
* [make](https://www.gnu.org/software/make/)

Additionally SSH Key-pairs will need to be generated and distributed to the provisioned EC2 instances.

### Install Ansible and Requirements

Update the apt repositories:

`apt update`

Install *software-properties-common*:

`apt install software-properties-common`

Add the official ansible PPA repository:

`apt-add-repository --yes --update ppa:ansible/ansible`

Install git, Ansible 2.7.7+:

`apt install ansible git make`

### Generate SSH Key

Generate a RSA 4096 bit key-pair:

`ssh-keygen -t rsa -b 4096`

### Clone the git repository

Change directories to where we will store the playbooks:

`cd /webapps/deployment/`

Clone the Ansible playbooks with the following command:

`git clone https://opensource.ieee.org/infra/playbooks`

### Deploy SSH Key

Change directories to the playbooks directory (if not done so already):

`cd /webapps/deployment/playbooks`

Deploy the SSH Key generated in the previous step into the desired environment (*dev*, *qa*, or *prod*), and pull any updates. In this example we will use *dev*, please do not forget to change *dev* to match the environment you are attempting copy SSH key-pairs into::

For Development:

`make keys-dev`

For QA:

`make keys-qa`

For Production:

`make keys-prod`

### Install AWS CLI v2

Change directories to the playbooks directory (if not done so already):

`cd /webapps/deployment/playbooks`

Install AWS CLI v2 into the desired environment (*dev*, *qa*, or *prod*), and pull any updates.

For Development:

`make aws-dev`

For QA:

`make aws-qa`

For Production:

`make aws-prod`

## Generate Secrets

Change directories to the playbooks directory (if not done so already):

`cd /webapps/deployment/playbooks`

To generate all secrets for the app, run the following script and answer the prompts.

For Development:

`make sec-dev`

For QA:

`make sec-qa`

For Production:

`make sec-prod`

## Running the Playbooks

Change directories to the playbooks directory:

`cd /webapps/deployment/playbooks`

Pull in any updates to the playbooks (*dev*, *qa*, or *prod*), and pull any updates. In this example we will use *dev*, please do not forget to change *dev* to match the environment you are attempting to run the Playbooks on.

For Development:

`make run-dev`

For QA:

`make run-qa`

For Production:

`make run-prod`
