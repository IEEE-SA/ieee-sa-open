---
title: "2. Upgrade"
weight: 1
description: >
  Install Process for SA Open Platform
---

When installing the SA Open Platform, we recommend you maintain a CIS Hardened Instance of Ubuntu 18.04 in order to run the Ansible Playbooks from

# Configure the Ansible Host

Change directories to the playbooks directory:

`cd playbooks`

Pull in any updates to the playbooks:

`git pull origin main`

Run the ansible playbooks:

`ansible .`
