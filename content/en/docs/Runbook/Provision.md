---
title: "0. Provision"
weight: 1
description: >
  Required resources to provision
---
	
# Amazon Web Services (AWS) Resources

## Elastic Compute Cloud (EC2)

Four EC2 Instances running [CIS Hardened Ubuntu 18.04 LTS](https://www.cisecurity.org/cis-hardened-images/amazon/).

| Name       | Type      | Description                                                                                |
|------------|-----------|--------------------------------------------------------------------------------------------|
| app        | t3.large  | [GitLab Application](https://gitlab.com) server                                            |
| git        | t3.medium | [Gitaly](https://gitlab.com/gitlab-org/gitaly) server                                      |
| mattermost | t3.medium | [MatterMost](https://mattermost.com) server                                                |
| tools      | t3.large  | [Grafana Dashboard](https://docs.gitlab.com/omnibus/settings/grafana.html) and other tools |

## Simple Storage Service (S3)

Three S3 buckets are required to deploy the SA Open Platform.

| Name      | Type        | Description                                                                                                                             |
|-----------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------|
| registry  | S3 Standard | Storage for [CI/CD container registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#use-object-storage)   |
| artifacts | S3 Standard | Storage for [CI/CD job artifacts](https://docs.gitlab.com/ee/administration/job_artifacts.html#using-object-storage)                    |
| backups   | S3 Standard | Storage for [application backups](https://docs.gitlab.com/ee/raketasks/backup_restore.html#uploading-backups-to-a-remote-cloud-storage) |

## Elastic Kubernetes Service (EKS)

A single *auto-scaling* EKS instance is required to run continuous integration / continuous deployment (CI/CD).

| Name | Type       | Nodes | Minium Nodes |
| ---  | ---        | ---   | ---          |
| cicd | AL2_x86_64 | 20    | 2            |

## Elastic Block Storage (EBS)

Two EBS instances are required.

| Name     | Size | Type                      | Mount Point   |
| ---      | ---  | ---                       | ---           |
| app-data | 200G | General Purpose SSD (GP2) | app:/adata    |
| git-data | 200G | General Purpose SSD (GP2) | gitaly:/gdata |

## Relational Database Service (RDS)
| Name | Size        | Type       |
| ---  | ---         | ---        |
| sgit | db.m4.large | PostgreSQL |

## ElastiCache (EK)

| Name | Size           | Type  |
| ---  | ---            | ---   |
| sgit | cache.m4.large | Redis |
