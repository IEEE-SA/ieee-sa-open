---
title: Maturity
menu:
  main:
      weight: 40
---
<style>
table {
	display: table !important;
	margin-right: 20px;
}
	th,td {
	border-left: 2px solid #dee2e6 !important;
    border-right: 2px solid #dee2e6 !important;
}
</style>

## Maturity Levels
 
 
* ![Scheduled](scheduled.svg) **Scheduled** -- Not yet offered on [IEEE SA Open](https://opensource.ieee.org), but on our [roadmap](/roadmap)
* ![Minimal](minimal.svg) **Minimal** -- Offered on [IEEE SA Open](https://opensource.ieee.org), but has not been rated by our community experience survey
* ![Viable](viable.svg) **Viable** -- Offered on [IEEE SA Open](https://opensource.ieee.org), and has been rated at least 50/100 on our user experience survey
* ![Complete](complete.svg) **Complete** -- Offered on [IEEE SA Open](https://opensource.ieee.org), and has been rated at least 75/100 on our user experience survey
* ![Lovable](lovable.svg)**Lovable** -- Offered on [IEEE SA Open](https://opensource.ieee.org), and has been rated at least 90/100 on our user experience survey

## Maturity

| Community                                               | Production Ready Code                                                       | Role Diversity                                            | Support                                                        |
|---------------------------------------------------------|-----------------------------------------------------------------------------|-----------------------------------------------------------|----------------------------------------------------------------|
| ![Minimal](minimal.svg) [Teams](#teams)                 | ![Scheduled](scheduled.svg) [Continual Integration](#continual-integration) | ![Scheduled](scheduled.svg) [GitLab Pages](#gitlab-pages) | ![Scheduled](scheduled.svg) [Service Desk](#service-desk)             |
| ![Scheduled](scheduled.svg) [Peer Review](#peer-review) | ![Scheduled](scheduled.svg) [Container Registry](#container-registry)       | ![Minimal](minimal.svg) [GitLab Issues](#gitlab-issues)   | ![Scheduled](scheduled.svg) [Staff Training](staff-training)   |
|                                                         | ![Scheduled](minimal.svg) [Repository Hosting](repository-hosting)          |                                                           | ![Scheduled](scheduled.svg) [Platform Deployment](#platform-deployment) |


## Maturity Breakdown

### Community

#### Teams

Unlock your teams potential with access to our platforms [Mattermost](https://mattermost.com/) community chat instance.  With access to our robust chat service your *team* will be able to:

* Securely communicate with Mattermost ([AICPA SOC2 Type 1 Compliant](https://mattermost.com/compliance/))
* Create a channels (chat rooms) to enable real time collaboration
* Exchange files with team members
* Receive real time updates on your repositories status

[Learn more...](docs/teams)

#### Peer Review

Collaborate with community members to receive feedback on your groups work. Leverage such features as:

* GitLab's [Code Review](https://about.gitlab.com/blog/2017/03/17/demo-mastering-code-review-with-gitlab/)

### Production Ready Code

#### Continual Integration

#### Container Registry


#### Repository Hosting

### Role Diversity

#### GitLab Pages

#### Gitlab Issues

### Support

#### Service Desk

#### Staff Training


