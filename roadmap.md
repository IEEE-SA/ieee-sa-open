# Projects

## 2.0.0

### Milestones

| Milestone | Description | Status | Deadline |
| ---       | ---         | ---    | ---      |
| Launch Production | Launch the production instance of GitLab Phase II to the IEEE and general public | Pending | Oct 23, 2020  |
| Launch QA/Testing Instance | Launch the QA and development isntaces of GitLab CE with CI/CD | Pending | Sep 28, 2020 |
| Launch Development Instance | Launch the development instance of GitLab CE with CI/CD | In Progress | Aug 21, 2020 |
| Onboarding | Onboard contractors to GitLab Phase II Related Resources. | Completed | Jun 5, 2020 |

